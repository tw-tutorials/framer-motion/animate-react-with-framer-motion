import { FC, useState } from 'react';
import { BrowserRouter, Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { AnimatePresence } from 'framer-motion';
import { Base, Pizza, Topping } from './types/pizza';
import { Header } from './components/layout/Header';
import { Modal } from './components/dialogs/Modal';
// pages
import { Home } from './pages/Home';
import { OrderBase } from './pages/order/Base';
import { OrderToppings } from './pages/order/Toppings';
import { OrderComplete } from './pages/order/Complete';

const App: FC = () => {
	const location = useLocation();
	const [pizza, setPizza] = useState<Pizza>({ base: '', toppings: [] });
	const [showModal, setShowModal] = useState<boolean>(false);

	const addBase = (base: Base) => {
		setPizza({ ...pizza, base });
	};

	const toggleTopping = (topping: Topping) => {
		let newToppings: Topping[];
		if (!pizza.toppings.includes(topping)) {
			newToppings = [...pizza.toppings, topping];
		} else {
			newToppings = pizza.toppings.filter((t) => t !== topping);
		}
		setPizza({ ...pizza, toppings: newToppings });
	};

	return (
		<>
			<Header />
			<Modal show={showModal} setShow={setShowModal} />

			<AnimatePresence exitBeforeEnter onExitComplete={() => setShowModal(false)}>
				<Switch location={location} key={location.key}>
					<Route exact path="/">
						<Home />
					</Route>

					<Route exact path="/order/base">
						<OrderBase pizza={pizza} addBase={addBase} />
					</Route>
					<Route exact path="/order/toppings">
						<OrderToppings pizza={pizza} toggleTopping={toggleTopping} />
					</Route>
					<Route exact path="/order/complete">
						<OrderComplete pizza={pizza} setShowModal={setShowModal} />
					</Route>

					<Redirect to="/" />
				</Switch>
			</AnimatePresence>
		</>
	);
};

export const AppWrapper: FC = () => (
	<BrowserRouter>
		<App />
	</BrowserRouter>
);
