import { FC } from 'react';
import { Link } from 'react-router-dom';
import { Base, bases, Pizza } from '../../types/pizza';
import { motion, Variants } from 'framer-motion';

const containerVariants: Variants = {
	hidden: {
		opacity: 0,
		x: '100vw',
	},
	visible: {
		opacity: 1,
		x: 0,
		transition: {
			type: 'spring',
			delay: 0.5,
		},
	},
	exit: {
		x: '-100vw',
		transition: {
			ease: 'easeInOut',
		},
	},
};

const nextVariants: Variants = {
	hidden: {
		x: '-100vw',
	},
	visible: {
		x: 0,
		transition: {
			type: 'spring',
			stiffness: 120,
		},
	},
};

const buttonVariants: Variants = {
	hover: {
		scale: 1.1,
		textShadow: '0px 0px 8px #fff',
		boxShadow: '0px 0px 8px #fff',
		transition: {
			duration: 0.3,
			yoyo: Infinity,
		},
	},
};

export const OrderBase: FC<{
	pizza: Pizza;
	addBase: (base: Base) => void;
}> = (props) => {
	const { pizza, addBase } = props;

	return (
		<motion.div
			className="base container"
			variants={containerVariants}
			initial="hidden"
			animate="visible"
			exit="exit"
		>
			<h3>Step 1: Choose Your Base</h3>
			<ul>
				{bases.map((base) => {
					let spanClass = pizza.base === base ? 'active' : '';
					return (
						<motion.li
							key={base}
							onClick={() => addBase(base)}
							whileHover={{ scale: 1.3, color: '#f8e112', originX: 0 }}
							transition={{ type: 'spring', stiffness: 300 }}
						>
							<span className={spanClass}>{base}</span>
						</motion.li>
					);
				})}
			</ul>

			{pizza.base && (
				<motion.div className="next" variants={nextVariants} initial="hidden" animate="visible">
					<Link to="/order/toppings">
						<motion.button variants={buttonVariants} whileHover="hover">
							Next
						</motion.button>
					</Link>
				</motion.div>
			)}
		</motion.div>
	);
};
