import clsx from 'clsx';
import { FC } from 'react';
import { Link } from 'react-router-dom';
import { Pizza, Topping, toppings } from '../../types/pizza';
import { motion, Variants } from 'framer-motion';

const containerVariants: Variants = {
	hidden: {
		opacity: 0,
		x: '100vw',
	},
	visible: {
		opacity: 1,
		x: 0,
		transition: {
			type: 'spring',
			delay: 0.5,
		},
	},
	exit: {
		x: '-100vw',
		transition: {
			ease: 'easeInOut',
		},
	},
};

const nextVariants: Variants = {
	hidden: {
		x: '-100vw',
	},
	visible: {
		x: 0,
		transition: {
			type: 'spring',
			stiffness: 120,
		},
	},
};

const buttonVariants: Variants = {
	hover: {
		scale: 1.1,
		textShadow: '0px 0px 8px #fff',
		boxShadow: '0px 0px 8px #fff',
		transition: {
			duration: 0.3,
			yoyo: Infinity,
		},
	},
};

export const OrderToppings: FC<{
	pizza: Pizza;
	toggleTopping: (topping: Topping) => void;
}> = (props) => {
	const { pizza, toggleTopping } = props;

	return (
		<motion.div
			className="container toppings"
			variants={containerVariants}
			initial="hidden"
			animate="visible"
			exit="exit"
		>
			<h3>Step 2: Choose Toppings</h3>
			<ul>
				{toppings.map((topping) => (
					<motion.li
						key={topping}
						onClick={() => toggleTopping(topping)}
						whileHover={{ scale: 1.3, color: '#f8e112', originX: 0 }}
						transition={{ type: 'spring', stiffness: 300 }}
					>
						<span
							className={clsx({
								active: pizza.toppings.includes(topping),
							})}
						>
							{topping}
						</span>
					</motion.li>
				))}
			</ul>

			<Link to="/order/complete">
				<motion.button variants={buttonVariants} whileHover="hover">
					Order
				</motion.button>
			</Link>
		</motion.div>
	);
};
