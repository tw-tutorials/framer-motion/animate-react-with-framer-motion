import { FC } from 'react';
import { Link } from 'react-router-dom';
import { motion, Variants } from 'framer-motion';
import { containerVariants } from '../animations/global';
import { Loader } from '../components/layout/Loader';

const buttonVariants: Variants = {
	hover: {
		scale: 1.1,
		textShadow: '0px 0px 8px #fff',
		boxShadow: '0px 0px 8px #fff',
		transition: {
			duration: 0.3,
			yoyo: Infinity,
		},
	},
};

export const Home: FC = () => {
	return (
		<motion.div
			className="container home"
			variants={containerVariants}
			initial="hidden"
			animate="visible"
			exit="exit"
		>
			<h2>Welcome to Pizza Joint</h2>

			<Link to="/order/base">
				<motion.button variants={buttonVariants} whileHover="hover">
					Create Your Pizza
				</motion.button>
			</Link>

			<Loader />
		</motion.div>
	);
};
