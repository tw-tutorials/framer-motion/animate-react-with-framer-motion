export type Base = 'Classic' | 'Thin & Crispy' | 'Thick Crust';
export const bases: Base[] = ['Classic', 'Thin & Crispy', 'Thick Crust'];

export type Topping = 'mushrooms' | 'peppers' | 'onions' | 'olives' | 'extra cheese' | 'tomatoes';
export const toppings: Topping[] = [
	'mushrooms',
	'peppers',
	'onions',
	'olives',
	'extra cheese',
	'tomatoes',
];

export interface Pizza {
	base: Base | '';
	toppings: Topping[];
}
