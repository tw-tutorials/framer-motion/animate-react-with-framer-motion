import { motion, useCycle, Variants } from 'framer-motion';
import { FC } from 'react';

import '../../styles/loader.scss';

const loaderVariants: Variants = {
	animation1: {
		x: [-20, 20],
		y: [0, -30],
		transition: {
			x: {
				yoyo: Infinity,
				duration: 0.5,
			},
			y: {
				yoyo: Infinity,
				duration: 0.25,
				ease: 'easeOut',
			},
		},
	},
	animation2: {
		y: [0, -40],
		x: 0,
		transition: {
			y: {
				yoyo: Infinity,
				duration: 0.25,
				ease: 'easeOut',
			},
		},
	},
};

export const Loader: FC = () => {
	const [animation, cycleAnimation] = useCycle('animation1', 'animation2');

	return (
		<>
			<motion.div className="loader" variants={loaderVariants} animate={animation}></motion.div>

			<div onClick={() => cycleAnimation()}>Cycle Loader</div>
		</>
	);
};
