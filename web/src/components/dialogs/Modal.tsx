import { Dispatch, FC, SetStateAction } from 'react';
import { motion, AnimatePresence, Variants } from 'framer-motion';
import { Link } from 'react-router-dom';

const backdropVariants: Variants = {
	hidden: {
		opacity: 0,
	},
	visible: {
		opacity: 1,
	},
};

const modalVariants: Variants = {
	hidden: {
		y: '-100vh',
		opacity: 0,
	},
	visible: {
		y: 200,
		opacity: 1,
		transition: {
			delay: 0.5,
		},
	},
};

export const Modal: FC<{ show: boolean; setShow: Dispatch<SetStateAction<boolean>> }> = (props) => {
	const { show, setShow } = props;

	return (
		<AnimatePresence exitBeforeEnter>
			{show && (
				<motion.div
					className="backdrop"
					variants={backdropVariants}
					initial="hidden"
					animate="visible"
					exit="hidden"
					onClick={(e) => {
						e.stopPropagation();
						setShow(false);
					}}
				>
					<motion.div className="modal" variants={modalVariants} initial="hidden" animate="visible">
						<p>Want to make another pizza?</p>
						<Link to="/order/base">
							<button>Start Again</button>
						</Link>
					</motion.div>
				</motion.div>
			)}
		</AnimatePresence>
	);
};
